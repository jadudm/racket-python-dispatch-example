# Purpose

To demonstrate calling a Racket web server from a Python script.

# Racket

To run, load the script into Dr Racket, and hit RUN. Or, on the command line:

``/Applications/Racket\ v6.9/bin/racket lookup-server.rkt``

(or wherever your racket executable lives)

# Python

You need to have virtualenv installed. Google.

Then, source the setup script:

``source setup.sh``

This installs the Requests library, which makes HTTP work easier/more fun. After that, on the command line:

``python lookup-client.py``

Then, you should see that the server will print a message, and the client will print a message. 

# Finally, We Hope

Huzzah!
